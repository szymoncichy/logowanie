// app/models/user.js
// zaladuj niezbedne narzedzia
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// utworz scheme naszego modelu
var userSchema = mongoose.Schema({

    local            : {
        nazwa        : String,
        password     : String,
    },
    facebook         : {
        id           : String,
        token        : String,
        name         : String,
        email        : String
    },
    twitter          : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String
    },
    google           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    }

});

// metody pomocnicze ======================
//wygeneruj hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// sprawdzanie czy haslo jest prawidlowe
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

// utworz model dla naszego uzytkownika i przekaz go do aplikacji
module.exports = mongoose.model('User', userSchema);