// app/routes.js
module.exports = (app, passport) => {

    app.get('/', (req, res) => {
        res.render('home', {
            user : req.user
        });
    });

    app.get('/sekret', (req, res) => {
        res.render('sekret', {
            user : req.user
        });
    });

    app.get('/login', (req, res) => {
        res.render('login', {
            message: req.flash('loginMessage')
        });
    });

    app.get('/rejestracja', (req, res) => {
        res.render('rejestracja', {
            message: req.flash('signupMessage')
        });
    });

    app.get('/profil', isLoggedIn, function(req, res) {
        res.render('profil', {
            user : req.user // pobierz uzytkownika z sesji i przekaz do szablonu
        });
    });

    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });


    app.post('/rejestracja', passport.authenticate('local-signup', {
        successRedirect : '/profil', // przekieruj na strone profilowa
        failureRedirect : '/rejestracja', // jesli blad przekieruj na strone rejestracji
        failureFlash : true // zezwalaj na wyskakujace wiadomosci (flash)
    }));

        // przetwarzanie formularza logowania
        app.post('/login', passport.authenticate('local-login', {
            successRedirect : '/profil', // przekieruj do zabezpieczonej podstrony
            failureRedirect : '/login', // jezeli blad przekieruj ponownie do strony logowania
            failureFlash : true // zezwalaj na wyskakujace wiadomosci
        }));
     
}

// middleware do przekierowania sprawdzajacy czy uzytkownik jest zalogowany
function isLoggedIn(req, res, next) {

    // jesli uzytkownik jest zalogowany dzialaj dalej
    if (req.isAuthenticated())
        return next();

    // jeśli nie przekieruj na stronę główną
    res.redirect('/');
}