// app.js

// narzędzia =====================================
// podłącz wszystkie narzędzia
const path = require("path");
const express = require('express');
const app = express();

const exphbs  = require('express-handlebars');

const mongoose = require('mongoose');
const passport = require('passport');
const flash = require('connect-flash');

const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');

const configDB = require('./config/baza.js');

 
// konfiguracja ============================================
mongoose.connect(configDB.url,{
    useMongoClient: true,
    /* other options */
  }); // Połącz z bazą

// konfiguracja expressa ====================================
app.use(morgan('dev')); // loguj kade zapytanie w konsoli
app.use(cookieParser()); // czytaj cistka - wymagane do uwierzytelniania
app.use(bodyParser()); // pobierz dane z formularza html

app.engine('handlebars', exphbs({defaultLayout: 'main'})); // konfiguracja handlebars
app.set('view engine', 'handlebars'); // ustaw silnik na handlebars
 
// konfiguracja pod passport ================================
app.use(session({ secret: 'owcaowcaowcaowca' })); // unikalne haslo do sesji
app.use(passport.initialize());
app.use(passport.session()); // sesja logowania
app.use(flash()); // connect-flash do przechowywania informacji i komunikatow

require('./config/passport')(passport); // pass passport for configuration

// ściezki ===================================================
require('./app/routes.js')(app, passport); 

// uruchom ====================================================
app.listen(3000 , (err)=>{
    console.log("Serwer działa na 3000");
})