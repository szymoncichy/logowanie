// config/passport.js
// wczytaj strategię
var LocalStrategy   = require('passport-local').Strategy;

// załaduj model uzytkownika
var User            = require('../app/models/user');

// udostepnij modul naszej aplikacji wykorzystując module.exports
module.exports = function(passport) {

    // =========================================================================
    // konfiguracja sesji passporta ==================================================
    // =========================================================================
    // niezbędne aby utzymać sesję uzytkownika
    // passport needs ability to serialize and unserialize users out of session

    // Zapisuje uzytkownika dla sesji
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // wykorzystuje zpaisanego uzytkownika w sesji
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });


    // =========================================================================
    // LOKALNA REJESTRACJA =====================================================
    // =========================================================================
    // Nasza strategia rejestrowania nazywa sie local-signup

    passport.use('local-signup', new LocalStrategy({
        // domyślnie strategia wykorzystuje nazwe uzytkownika i haslo, 
        // zmienimy to aby jako login wykorzystywal adres email
   
        usernameField : 'nazwa',
        passwordField : 'password',
        passReqToCallback : true // pozwala na wyslanie zapytania w callbacku
    },
    function(req, nazwa, password, done) {

        // asynchronicznie
        // User.findOne nie odpali się dopuki nie otrzyma danych zwrotnych
        process.nextTick(function() {

        // znajdz uzytkownika jest taka jak email z formularza
        // sprawdzamy czy taki uzytkownik w ogole istnieje

        User.findOne({ 'local.nazwa' :  nazwa }, function(err, user) {
            // jezeli jakis blad zwroc blad
            if (err)
                return done(err);

            // sprawdz czy istnieje uzytkownik o podanym adresie email
            if (user) {
                return done(null, false, req.flash('signupMessage', 'Nazwa jest już zajęta.'));
            } else {

                // jeśli nie istnieje uzytkownik o podanym email
                // utwórz nowego uzytkowniak
                var newUser            = new User();

                // ustaw parametry uzytkownika
                newUser.local.nazwa    = nazwa;
                newUser.local.password = newUser.generateHash(password);

                // zapisz uzytkownika
                newUser.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, newUser);
                });
            }
            
        });    
        });

    }));

    // =========================================================================
    // LOKALNE LOGOWANIE=============================================================
    // =========================================================================
    // Naszą lokalną strategię logowania nazwiemy 'local-login'

    passport.use('local-login', new LocalStrategy({
        // domyślnie strategia logowania wykorzystuje nazwę uzytkownika i haslo
        // zmienimy ja na adres email
        usernameField : 'nazwa',
        passwordField : 'password',
        passReqToCallback : true 
    },
    function(req, nazwa, password, done) { // callback z adresem email i haslem z naszego formularza

        // znajdz uzytkownika ktorego email jest taki jak w formularzu
        User.findOne({ 'local.nazwa' :  nazwa }, function(err, user) {
            // jezeli sa bledy to zwroc bledy
            if (err)
                return done(err);

            // jesli nie znaleziono uzytkownika zwroc komunikat
            if (!user)
                return done(null, false, req.flash('loginMessage', 'Nie znaleziono uytkownika.')); // req.flash jest sposobem do przekazywania wiadomosci przy uzyciu paczki connect-flash

            // jesli uzytkownik istnieje ale haslo jest zle
            if (!user.validPassword(password))
                return done(null, false, req.flash('loginMessage', 'Ups złe hasło.')); // utworz wiadomosc i dodaj ja jako przekazywana wiadomosc

            // jesli wszystko dobrze zwroc uzytkownika
            return done(null, user);
        });

    }));


};